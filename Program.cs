﻿using System;
using System.Net;
using System.Threading;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Telegram.Bot;
using Telegram.Bot.Args;
using Newtonsoft.Json;

namespace telegramYoutubeBot
{
    public struct PageInfo
    {
        public int totalResults { get; set; }
        public int resultsPerPage { get; set; }
    }
    public struct Snippet
    {
        public string title { get; set; }
        public string channelTitle { get; set; }
    }
    public struct Id
    {
        public string kind { get; set; }
        public string videoId { get; set; }
    }
    public struct Item
    {
        public Id id { get; set; }
        public Snippet snippet { get; set; }
    }
    public struct Root
    {
        public PageInfo pageInfo { get; set; }
        public Id id { get; set; }
        public List<Item> items { get; set; }
    }

    class Program
    {
        readonly static string telegramToken = "TOKEN"; // telegram token
        private static TelegramBotClient client; // creating telegram client variable
        private static readonly string ApiKey = "APIKEY"; // youtube token
        readonly static string pattern = @"^(https?:\/\/)?(\w+\.)?youtube\.com\/(channel)\/(.{24})"; // regexp pattern

        [Obsolete]
        static void Main(string[] args)
        {
            client = new TelegramBotClient(telegramToken);
            client.StartReceiving();
            client.OnMessage += OnMessageHandler;
            Console.ReadLine();
            client.StopReceiving();
        }

        [Obsolete]
        private static async void OnMessageHandler(object sender, MessageEventArgs e)
        {
            var msg = e.Message;
            if (msg.Text != null)
                Console.WriteLine($"[LOG] Пришло сообщение с текстом: \"{msg.Text}\" от {msg.From.FirstName + " " + msg.From.LastName}");

            if (Regex.IsMatch(msg.Text, pattern, RegexOptions.IgnoreCase))
            {
                foreach (Match m in Regex.Matches(msg.Text, pattern, RegexOptions.IgnoreCase))
                {
                    await client.SendTextMessageAsync(msg.Chat.Id, $"Канал {m.Groups[4].Value} был добавлен в отслеживаемые!");
                    Console.WriteLine($"[LOG] Канал {m.Groups[4].Value} был добавлен в отслеживаемые!");

                    var streamTask = new WebClient().DownloadString($"https://www.googleapis.com/youtube/v3/search?part=snippet&channelId={m.Groups[4].Value}&maxResults=10&order=date&type=video&key={ApiKey}"); // getting json
                    Root info = JsonConvert.DeserializeObject<Root>(streamTask); // converting json into object
                    var channelId = m.Groups[4].Value;
                    var result = info.pageInfo.totalResults;
                    
                    while (true)
                    {
                        var isNewVideo = CheckForNewVideo(ref result, channelId); // return true or false
                        
                        if (isNewVideo)
                            await client.SendTextMessageAsync(msg.Chat.Id, $"Вышло новое видео на канале {GetValues.Get((channelId), ApiKey, "channelName")}!\n\n" + 
                            $"Название: {GetValues.Get((channelId), ApiKey, "videoTitle")}\n\nСсылка: https://www.youtube.com/watch?v={GetValues.Get((channelId), ApiKey, "newVideo")}",
                            replyToMessageId:msg.MessageId);
                        Thread.Sleep(1800000); // 30 minutes
                    }
                }
            }
            else
                await client.SendTextMessageAsync(msg.Chat.Id, "Отправьте ссылку на канал в виде \"https://www.youtube.com/channel/UChqcJ_MhP9a4bXy1jQ0QPzQ\"");
        }

        private static bool CheckForNewVideo(ref int videosCount, string id)
        {
            var streamTask = new WebClient().DownloadString($"https://www.googleapis.com/youtube/v3/search?part=snippet&channelId={id}&maxResults=10&order=date&type=video&key={ApiKey}"); // getting json
            Root root = JsonConvert.DeserializeObject<Root>(streamTask); // converting json into object
            
            if (videosCount < root.pageInfo.totalResults) // if the initial value is less then return true
            {
                videosCount = root.pageInfo.totalResults;
                return true;
            }
            else
                return false;
        }
    }
}
