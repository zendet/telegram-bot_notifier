using System.Net;
using Newtonsoft.Json;

namespace telegramYoutubeBot
{
    class GetValues
    {
        public static string Get(string id, string apiKey, string request)
        {
            var streamTask = new WebClient().DownloadString($"https://www.googleapis.com/youtube/v3/search?part=snippet&channelId={id}&maxResults=10&order=date&type=video&key={apiKey}"); // getting json
            Root root = JsonConvert.DeserializeObject<Root>(streamTask); // converting json into object
            switch (request)
            {
                case "videoTitle":
                    foreach (var c in root.items) return c.snippet.title;
                    break;
                case "channelName":
                    foreach (var c in root.items) return c.snippet.channelTitle;
                    break;
                case "newVideo":
                    foreach (var c in root.items) return c.id.videoId;
                    break;
            }
            return "";
        }
    }
}